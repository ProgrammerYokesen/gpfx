<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>GPFX</title>

    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous" />
</head>

<body>
    <!-- HEADER -->
    <div class="header_container">
        <div class="container">
            <div class="logo" id="logo"></div>
            <div class="menu">
                <a href="#" class="menu_klasemen">KLASEMEN</a>
                <button>MASUK</button>
            </div>
        </div>
    </div>

    <!-- HERO / JUMBOTRON -->
    <div class="hero_container">
        <div class="container">
            <h1>GRANDPRIX</h1>
            <h2>Series Trading Indonesia</h2>
            <h5>
                Ikuti dan menangkan hadiah total senilai 70 juta rupiah! Sudah pasti
                akan ada pemenang setiap minggunya dan juga nantikan hadiah sebagai
                juara umum yang akan diumumkan di akhir periode grand prix series
                trading indonesia!
            </h5>
            <button>Daftar Sekarang</button>
        </div>
    </div>

    <!-- TABLE -->
    <div class="table_container">
        <div class="container">
            <h2>TOTAL HADIAH GRAND PRIX</h2>
            <ul class="nav nav-pills justify-content-center mb-3" id="pills-tab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link active" id="pills-weekly-tab" data-toggle="pill" href="#pills-weekly" role="tab"
                        aria-controls="pills-weekly" aria-selected="true">WEEKLY</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="pills-champion-tab" data-toggle="pill" href="#pills-champion" role="tab"
                        aria-controls="pills-champion" aria-selected="false">CHAMPION</a>
                </li>
            </ul>

            <div class="tab-content" id="pills-tabContent">
                <!-- WEEKLY TABLE -->
                <div class="tab-pane fade show active table-responsive" id="pills-weekly" role="tabpanel"
                    aria-labelledby="pills-weekly-tab">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">Type</th>
                                <th scope="col">Juara</th>
                                <th scope="col">Point Grand Prix</th>
                                <th scope="col">Hadiah Mingguan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">Grand Prix Series</th>
                                <td>1</td>
                                <td>25</td>
                                <td>Rp1.000.000,-</td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td>2</td>
                                <td>18</td>
                                <td>Rp1.000.000,-</td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td>3</td>
                                <td>15</td>
                                <td>Rp1.000.000,-</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <!-- CHAMPION TABLE -->
                <div class="tab-pane fade table-responsive" id="pills-champion" role="tabpanel"
                    aria-labelledby="pills-champion-tab">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">Type</th>
                                <th scope="col">Juara</th>
                                <th scope="col">Point Grand Prix</th>
                                <th scope="col">Hadiah Mingguan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">Grand Prix Series</th>
                                <td>1</td>
                                <td>25</td>
                                <td>Rp1.000.000,-</td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td>2</td>
                                <td>18</td>
                                <td>Rp1.000.000,-</td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td>3</td>
                                <td>15</td>
                                <td>Rp1.000.000,-</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END TABLE -->

    <!-- HOW TO JOIN -->
    <div class="join_container">
        <div class="container">
            <h2>CARA BERGABUNG</h2>
            <img src="{{ asset('images/join.png') }}" alt="cara join" class="join-d" />
            <img src="{{ asset('images/join-mobile.svg') }}" alt="cara join" class="join-mb" />
            <div class="text-center">
                <button>DAFTAR SEKARANG</button>
            </div>
        </div>
    </div>
    <!-- END HOW TO JOIN -->

    <!-- FAQ -->
    <div class="faq_container">
        <div class="container">
            <h2>Frequently Asked Questions</h2>

            <!-- FAQ 1 -->
            <div class="question_container" data-toggle="collapse" href="#faq1" role="button" aria-expanded="false"
                aria-controls="faq1">
                <h5>Kapan pemenang dari Grand Prix Series akan diumumkan?</h5>
                <img src="{{ asset('images/plus.svg') }}" alt="expand" />
            </div>
            <div class="collapse faq_answer" id="faq1">
                <div>
                    <h5 class="mb-4">
                        Grand Prix Series weekly akan diumumkan pada hari Sabtu setiap
                        minggunya di closing market.
                    </h5>
                    <h5>
                        Grand Prix Series total akan diumumkan pada tanggal 24 Juli 2021
                        setelah 15 series telah selesai diadakan.
                    </h5>
                </div>
            </div>
            <hr />
            <!-- END FAQ 1 -->

            <!-- FAQ 2 -->
            <div class="question_container" data-toggle="collapse" href="#faq2" role="button" aria-expanded="false"
                aria-controls="faq2">
                <h5>Berapa lama Grand Prix Series ini akan berlangsung?</h5>
                <img src="{{ asset('images/plus.svg') }}" alt="expand" />
            </div>
            <div class="collapse faq_answer" id="faq2">
                <div>
                    <h5 class="mb-4">
                        Grand Prix mingguan akan diadakan selama 15 minggu yang terdiri
                        dari 1 series dengan pemilihan pair yang berbeda-beda setiap
                        minggunya. Berikut adalah jadwal dari Grand Prix Series mingguan:
                    </h5>
                    <div class="faq_jadwal">
                        <img src="{{ asset('images/jadwal-1.png') }}" alt="jadwal" />
                        <img src="{{ asset('images/jadwal-2.png') }}" alt="jadwal" />
                        <img src="{{ asset('images/jadwal-3.png') }}" alt="jadwal" />
                    </div>
                </div>
            </div>
            <hr />
            <!-- END FAQ 2 -->

            <!-- FAQ 3 -->
            <div class="question_container" data-toggle="collapse" href="#faq3" role="button" aria-expanded="false"
                aria-controls="faq3">
                <h5>Siapa yang dapat memenangkan Grand Prix Series?</h5>
                <img src="{{ asset('images/plus.svg') }}" alt="expand" />
            </div>
            <div class="collapse faq_answer" id="faq3">
                <div>
                    <h5 class="mb-4">
                        Pemenang akan ditentukan berdasarkan klasemen terakhir di setiap
                        minggu. Perhitungan klasemen akan berurutan berdasarkan poin yang
                        didapatkan dari ROI (Return On Investment), dan perhitungan ROI
                        akan kembali menjadi 0% di setiap minggunya.
                    </h5>
                    <h5>
                        Hanya transaksi dipair yang sesuai dengan jadwal pair yang telah
                        ditentukan yang akan dihitung sebagai ROI (Return Of Investment)
                        untuk perolehan klasemen dan Transaksi di luar pair yang ada di
                        jadwal setiap seriesnya tidak akan dihitung sebagai ROI (Return Of
                        Investment) untuk klasemen.
                    </h5>
                </div>
            </div>
            <hr />
            <!-- END FAQ 3 -->

            <!-- FAQ 4 -->
            <div class="question_container" data-toggle="collapse" href="#faq4" role="button" aria-expanded="false"
                aria-controls="faq4">
                <h5>Siapa yang dapat memenangkan Grand Champion?</h5>
                <img src="{{ asset('images/plus.svg') }}" alt="expand" />
            </div>
            <div class="collapse faq_answer" id="faq4">
                <div>
                    <h5 class="mb-4">
                        Peserta mengumpulkan point Grand Prix dari series mingguan untuk
                        diakumulasikan dalam perolehan point sebagai Grand Champion
                        diakhir periode
                    </h5>
                </div>
            </div>
            <hr />
            <!-- END FAQ 4 -->
        </div>
    </div>
    <!-- END FAQ -->

    <!-- FOOTER -->
    <div class="footer"></div>
    <!-- END FOOTER -->

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
    </script>

    <script>
        $(function() {
            $(document).scroll(function() {
                var $nav = $(".header_container");
                var $menu = $(".menu_klasemen");
                var $logo = $("#logo");

                $nav.toggleClass("scrolled", $(this).scrollTop() > $nav.height());
                $menu.toggleClass("scrolled", $(this).scrollTop() > $nav.height());

                if ($(window).scrollTop() > $nav.height()) {
                    $logo.removeClass("logo").addClass("logo-scrolled");
                } else {
                    $logo.removeClass("logo-scrolled").addClass("logo");
                }
            });
        });

    </script>
</body>

</html>
